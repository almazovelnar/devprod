const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .styles([
            'resources/css/bootstrap.min.css',
            'resources/css/style.css'
        ],
        'public/assets/css/app.css'
    )
    .scripts([
            'resources/js/jquery.js',
            'resources/js/bootstrap.min.js',
        ],
        'public/assets/js/app.js'
    )
    .sourceMaps()
    .version();
