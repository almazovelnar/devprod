<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function form()
    {
        return view('auth.login');
    }

    public function index()
    {
        return view('home');
    }
}
