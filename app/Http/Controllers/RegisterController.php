<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class RegisterController extends Controller
{
    use AuthenticatesUsers;

    public function form()
    {
        return view('auth.register');
    }

    public function index()
    {
        return view('home');
    }
}
