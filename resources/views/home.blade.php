@extends('layout')

@section('content')
    <a href="{{ route('login') }}" class="btn btn-success text-white">Login</a>
    <a href="{{ route('register') }}" class="btn btn-primary text-white">Sign up</a>
@endsection
